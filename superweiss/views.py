# This file is part of Superweiss.
#
# Superweiss is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Superweiss is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Superweiss.  If not, see <http://www.gnu.org/licenses/>.

from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.models import User
from django.core import serializers
from django.db import transaction
from django.db.models import Sum, Q
from django.http import Http404, HttpResponse
from django.shortcuts import render, redirect, get_object_or_404
from django.urls import reverse_lazy
from django.utils.translation import gettext as _
from django.views.decorators.cache import cache_page
from django.views.generic import TemplateView, DetailView, ListView, \
    CreateView, DeleteView
from superweiss.models import Term, Person, Examination, Supervision, Backup

class DashboardView(TemplateView):
    """The Homepage view"""
    template_name = "home.html"

    def get_context_data(self, **kwargs):
        context = super(DashboardView, self).get_context_data(**kwargs)

        context['cur_term'] = Term.get_current_term()
        exams = Examination.objects.filter(term=context['cur_term'])
        context['num_exams'] = exams.count()
        context['num_supervisors'] = exams.aggregate(Sum('num_supervisors'))['num_supervisors__sum']
        tot_sup_h = 0
        for e in exams:
            try:
                tot_sup_h += e.num_supervisors*(e.endtime-e.starttime).seconds/60
            except TypeError:
                # no start/endtime was given
                pass
        tot_sup_h /= 60 # Make it hours
        supervisors = Person.objects.filter(supervisor=True, user__is_active=True)
        sums = supervisors.aggregate(Sum('deputat'),Sum('uebertrag'))
        tot_dep = sums['deputat__sum']
        context['tot_uebertrag'] = sums['uebertrag__sum']
        context['num_wimi'] = supervisors.count()
        # Add .1 as buffer
        context['sup_per_dep'] = (0.1 + (tot_sup_h + context['tot_uebertrag']) / tot_dep) if tot_dep else 0

        context['num_supervisors_h'] = tot_sup_h
        context['num_dep'] = tot_dep
        return context

    def as_cached_view():
        #raise Exception(request)
        return cache_page(60 * 15)(DashboardView.as_view())


def personen(request):
    """Show list of all persons involved"""
    return render(request, "personen_list.html")

class PersonListView(LoginRequiredMixin, ListView):
    model = Person
    template_name = "superweiss/person_list.html"
    queryset = Person.objects.filter(user__is_active=True, supervisor=True)

    def get_context_data(self, **kwargs):
        context = super(PersonListView, self).get_context_data(**kwargs)
        #context['now'] = timezone.now()
        return context


class PersonDetailView(LoginRequiredMixin, DetailView):
    model = User
    slug_field = "username"
    template_name = "superweiss/person_detail.html"

    def get(self, request, *args, **kwargs):
        pk = self.kwargs.get(self.pk_url_kwarg, None)
        slug = self.kwargs.get(self.slug_url_kwarg, None)
        if pk is None and slug is None:
                self.kwargs[self.pk_url_kwarg] = request.user.pk
        return super(PersonDetailView, self).get(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(PersonDetailView, self).get_context_data(**kwargs)
        cur_term = Term.get_current_term()
        context['cur_term'] = cur_term
        exams = Examination.objects.filter(term=cur_term)
        myexams = (exams.filter(supervision__person=self.object) | exams.filter(backup__person=self.object))
        context['examination_list']= myexams#exams.filter(supervision__person=self.object)
        return context


class ExamListView(LoginRequiredMixin, ListView):
    model = Examination
    cur_term = Term.get_current_term()

    def get_queryset(self):
        exams= Examination.objects.filter(term=self.cur_term)
        return exams
    
    def get_context_data(self, **kwargs):
        context = super(ExamListView, self).get_context_data(**kwargs)
        context['cur_term'] = self.cur_term
        return context

class ExamDetailView(LoginRequiredMixin, DetailView):
    model = Examination

    def get_context_data(self, **kwargs):
        context = super(ExamDetailView, self).get_context_data(**kwargs)
        return context

@login_required
def supervision_create(request, ex, backup=False):
    """Create a new supervision for exam with id `ex`.

    Signed in staff can append ?user=<username> to sign up other people.
    ::params:: backup: whether to create a backup supervision."""
    # TODO Add Sanity check that one cannot sign up simultanously
    person = request.user
    # After handling return to requested page or to exam_list
    next = request.GET.get('next', default='exam_list')

    try:
        exam = Examination.objects.get(id=ex)
    except Examination.DoesNotExist:
        # Translators: Msg when trying to sign up for an invalid examination id.
        messages.error(request, _('The examination you try to sign up for does not exist.'))
        exam = None

    #Only staff is allowed to sign up other people for the exam, the
    #link is not publically advertized anywhere either.
    if request.user.is_staff:
        # use different user if specified as ?user= or use ourselves
        alt_username = request.GET.get('user')
        if alt_username:
            try:
                person = User.objects.get(username=alt_username)
            except User.DoesNotExist:
                person = None
                messages.error(request, 'The username \'{}\' you try to sign up does not exist.'.format(request.GET.get('person')))


    if not (exam is None or person is None):
        # See if we are signed up for Supervision or Backup already
        sup = Supervision.objects.filter(
            Examination=exam, person=person).exists()
        back = Backup.objects.filter(Examination=exam, person=person).exists()

        if sup or back:
            # We are already signed up! Notify user.
            messages.warning(request, 'Person \'{}\' was already signed up for \'{}\'.'.format(person, exam))

        else:
            # Not signed up, so let's do that now.
            if backup: SupModel = Backup
            else: SupModel = Supervision

            # We want to create an atomic transaction, so we look the
            # database table, so that can protect from too many people
            # signing up for an exam..
            with transaction.atomic():            
                sup = SupModel.objects.create(Examination=exam, person=person)
                num_sup = SupModel.objects.filter(Examination=exam).count()
                if (backup and num_sup > exam.num_backups) \
                  or (not backup and num_sup > exam.num_supervisors):
                    sup.delete()
                    messages.warning(request,
                          'Sorry, all slots are already full for \'{}\'.'\
                          .format(exam))

                else:
                    sup.save()
                    messages.info(request,
                          'Successfully signed up \'{}\' for \'{}\'.'\
                          .format(person.person, exam))
    
    return redirect(next)


class SupervisionDeleteView(LoginRequiredMixin, DeleteView):
    model = Supervision
    success_url = reverse_lazy('exam_list')

    def get_success_url(self):
        """Honor ?next=... URL after deletion"""
        success_url = self.request.GET.get('next', None)
        if success_url is None:
            return self.success_url
        return success_url

class BackupDeleteView(SupervisionDeleteView):
    model = Backup
    template_name = "superweiss/supervision_confirm_delete.html"


class API_data_export(LoginRequiredMixin, ListView):
    # export examinations as JSON
    # TODO: introduce version field, spell out lecturers, etc?
    # TODO: at least also export exams, supervisors, and chairs too.
    model = Examination

    def get(self, request):
        exam_data = serializers.serialize("json", self.get_queryset())
        return HttpResponse(exam_data, content_type='application/json')

