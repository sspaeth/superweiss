# -*- coding: utf-8 -*-
# This file is part of Superweiss.
#
# Superweiss is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Superweiss is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Superweiss.  If not, see <http://www.gnu.org/licenses/>.

from django.contrib.auth.models import User
from django.urls import reverse
from django.db import models, OperationalError

# Monkeypatch "User" to display a nice human readable version.
def get_nice_name(self):
    if self.last_name and self.first_name:
        return "{} {}".format(self.first_name, self.last_name)
    elif self.last_name and not self.first_name:
        return "{} ({})".format(self.last_name, self.username)
    else: return self.username
User.get_nice_name = get_nice_name

class DefaultsAndValues(models.Model):
    """key-value pairs for saving as default settings, and cached values,
       which are saved by the system"""
    key = models.CharField(max_length=100, unique=True)
    value = models.CharField(max_length=100, blank=True, null=True)

    def __str__(self):
        return self.key

    class Meta:
        ordering = ['key']


class Term(models.Model):
    """Contains term names, such as "Summer Term (2014)" """
    name = models.CharField(max_length=50)
    year = models.PositiveSmallIntegerField()
    slug = models.SlugField(blank=True, unique=True)

    @classmethod
    def get_current_term(cls):
        """Returns the current term"""
        try:
            cur_term_pk = DefaultsAndValues.objects.get(key="current_term")
            term =  Term.objects.get(pk = cur_term_pk.value)
            return term
        except DefaultsAndValues.DoesNotExist:
            # admin has not yet set a current term, so we do that for her.
            try:
                # use the latest term that we can find
                term = Term.objects.all().latest()
            except Term.DoesNotExist:
                # WOW, not even a single TERM has been defined
                # yet. Create a temporary one for our admin that she
                # will have to rename later.
                term = Term(name="RENAME_ME",year=0,slug="RENAME_ME")
                term.save()
            default = DefaultsAndValues(key="current_term", value=term.pk)
            default.save()
            return term
        except OperationalError as e:
            if e.args[0].startswith("no such table:"):
                # The table DefaultsAndValues does not exist yet, so
                # we do not save anything. THis happens before the
                # first migration has finished.
                return None
        # We SHOULD never reach this point of desperation...
        raise Exception("Not able to determine the current term. Please check.")

    def __str__(self):
        return self.name

    class Meta:
        get_latest_by = "year"
        ordering = ['year', 'name']


class Chair(models.Model):
    name = models.CharField(max_length=100, unique=True)

    def __str__(self):
        return self.name

    class Meta:
        ordering = ['name']


class Person(models.Model):
    """Profile of a person, complementing a User

    Notable attributes: user.supervision_set --> All Supervision's this person signed up for"""
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    chair = models.ForeignKey(Chair, on_delete=models.CASCADE, null=True)
    deputat = models.FloatField(default=0)
    uebertrag = models.FloatField(default=0)
    supervisor = models.BooleanField(default=True)
    phone = models.CharField(max_length=100, null=True, blank=True)
    comment = models.TextField(blank=True)

    def supervision_h_required(self):
        #TODO need to cache and only calc when it changes?
        try:
            sup_per_dep = float(DefaultsAndValues.objects.get(key="supervision_per_deputat").value)
        except DefaultsAndValues.DoesNotExist: return 0
        return sup_per_dep*self.deputat + 0.1 # include security margin

    def supervision_h_signed_up(self, include_uebertrag=True):
        #TODO need to cache and only calc when it changes?
        """Returns the number of supervision hours"""
        h = (self.user.supervision_set.filter(
             Examination__term=Term.get_current_term()).aggregate(
             models.Sum('Examination__duration'))['Examination__duration__sum'] or 0) / 60
        if include_uebertrag:
            h += self.uebertrag
        return h

    def supervision_h_signed_up_no_uebertrag(self):
        #TODO need to cache and only calc when it changes?
        return self.supervision_h_signed_up(include_uebertrag=False)

    def num_backups(self):
        """Returns the number of backup that Person has signed up for in the
           current term"""
        return self.user.backup_set.filter(
             Examination__term=Term.get_current_term()).count()

    def backups_shall(self):
        """How many backups this person is supposed to do

        This wil usually be the same value for all persons"""
        return 2

    def __str__(self):
        return str(self.user)


class Examination(models.Model):
    """Represents a examination

    Notable attributes: .supervision_set --> A set of all Supervisions related to this exam"""
    term = models.ForeignKey(Term, default=Term.get_current_term, null=True,
                             on_delete=models.CASCADE)
    subject = models.CharField(max_length=100)
    lecturer = models.ForeignKey(User, on_delete=models.CASCADE)
    starttime = models.DateTimeField(null=True, blank=True)
    endtime = models.DateTimeField(null=True, blank=True)
    duration = models.PositiveSmallIntegerField(default=0, editable=False)
    rooms = models.CharField(max_length=100, blank=True)
    num_supervisors = models.PositiveSmallIntegerField(default=1)
    num_backups = models.PositiveSmallIntegerField(default=1)
    comment = models.TextField(blank=True)

    def signup_needed(self):
        """indicates whether we need more signups for this exam"""
        return self.supervision_set.all().count() < self.num_supervisors

    def backups_needed(self):
        """indicates whether we need more backup signups for this exam"""
        return self.backup_set.all().count() < self.num_backups

    def get_absolute_url(self):
        return reverse('exam_detail', args=[str(self.id)])

    def __str__(self):
        return "{} ({})".format(self.subject, self.term)

    class Meta:
        ordering = ['starttime']
        unique_together = (("term", "subject","lecturer"),)


class Supervision(models.Model):
    Examination = models.ForeignKey(Examination, on_delete=models.CASCADE)
    person = models.ForeignKey(User, on_delete=models.CASCADE)

    def get_absolute_url(self):
        return reverse('exam_detail', args=[str(self.Examination.id)])

    def __str__(self):
        return "{} - {}".format(self.Examination, self.person)

    class Meta:
        unique_together = (("Examination", "person"),)

class Backup(models.Model):
    Examination = models.ForeignKey(Examination, on_delete=models.CASCADE)
    person = models.ForeignKey(User, on_delete=models.CASCADE)

    def get_absolute_url(self):
        return reverse('exam_detail', args=[str(self.Examination.id)])

    def __str__(self):
        return "{} - {}".format(self.Examination, self.person)

    class Meta:
        unique_together = (("Examination", "person"),)



################################################################
### Signals
################################################################

# Signal to automatically create a user profile when a new user is created
def create_user_profile(sender, instance=None, created=None, **kw):
    if not created:
        return
    p = Person()
    p.user = instance
    p.save()
models.signals.post_save.connect(create_user_profile, sender=User,
                                 dispatch_uid="create_new_profile_hook")

def update_examination(sender, instance, **kw):
    """calculate values of the Examination object on save."""
    # calculate the total supervision requirement in hours
    exam = instance
    try:
        exam.duration = (exam.endtime-exam.starttime).seconds/60
    except TypeError:
        # no start/endtime was given
        exam.duration = 0
models.signals.pre_save.connect(update_examination, sender=Examination,
                                 dispatch_uid="update_examination")

def update_stats(sender, instance, **kw):
    """calculate and cache important values"""
    # calculate the total supervision requirement in hours
    cur_term = Term.get_current_term()
    exams = Examination.objects.filter(term=cur_term)
    tot_sup_h = 0
    for e in exams:
        try:
            tot_sup_h += e.num_supervisors*(e.endtime-e.starttime).seconds/60
        except TypeError:
            # no start/endtime was given
            pass
    tot_sup_h /= 60 # Make it hours
    supervisors = Person.objects.filter(supervisor=True, user__is_active=True)
    sums = supervisors.aggregate(models.Sum('deputat'),models.Sum('uebertrag'))
    tot_dep = sums['deputat__sum']
    tot_uebertrag = sums['uebertrag__sum']
    # Add .1 as buffer
    supervision_per_deputat= ((tot_sup_h + tot_uebertrag) / tot_dep) if tot_dep else 0
    obj, created = DefaultsAndValues.objects.get_or_create(
        key="supervision_per_deputat")
    obj.value = str(supervision_per_deputat)
    obj.save()

# call update_stats when: A person profile, an Examination, or an Supervision has been saved
models.signals.post_save.connect(update_stats, sender=Person,
                                 dispatch_uid="update_stats")
models.signals.post_save.connect(update_stats, sender=Examination,
                                 dispatch_uid="update_stats")
models.signals.post_save.connect(update_stats, sender=Supervision,
                                 dispatch_uid="update_stats")


def supervision_sanity_check(sender, instance, **kw):
    """Ensure that we don't have more supervisors than slots.

    Not really needed now, as we sanity check this in the view
    supvision_create.
    """
    exam = instance.Examination
    if exam.num_supervisors < exam.supervision_set.count():
     raise ValidationError(
        "Number of supervisors exceeds number of possible supervisors %(value)s.",
        code="exceeded_supervisors",
        params={'value': 'XXX'},
)
models.signals.pre_save.connect(supervision_sanity_check, sender=Supervision,
                                 dispatch_uid="supersivion_sanity")
