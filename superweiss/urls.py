# This file is part of Superweiss.
#
# Superweiss is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Superweiss is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Superweiss.  If not, see <http://www.gnu.org/licenses/>.

from django.conf.urls import include
from django.urls import path, re_path
from django.contrib import admin
from django.contrib.auth import views as auth_views

from superweiss.views import DashboardView, PersonListView, PersonDetailView, \
    ExamListView, ExamDetailView, SupervisionDeleteView,\
    BackupDeleteView, API_data_export, supervision_create


app_name = 'superweiss'
urlpatterns = [
    path('dashboard/', DashboardView.as_cached_view(), name='dashboard'),
    path(r'u', PersonListView.as_view(), name='person_list'),
    # Look up single persons by username
    path('u/id:<int:pk>/', PersonDetailView.as_view(), name='person_detail'),
    path('u/<username>/', PersonDetailView.as_view(), name='person_detail'),

    path(r'', ExamListView.as_view(), name='exam_list'),
    path('e/<int:pk>/', ExamDetailView.as_view(), name='exam_detail'),
    path('e/<int:ex>/signup/', supervision_create, kwargs={'backup': False},
        name='exam_signup'),
#    path('e/<int:ex>/signup/', SupervisionCreateView.as_view(),
#        name='exam_signup'),
    path('e/<int:ex>/s/<int:pk>/cancel/',
        SupervisionDeleteView.as_view(), name='supervision_cancel'),
    path('e/<int:ex>/b/signup/', supervision_create, kwargs={'backup': True},
         name='backup_signup', ),
    path('e/<int:ex>/b/<int:pk>/cancel/',
        BackupDeleteView.as_view(), name='backup_cancel'),

    path('api/export/', API_data_export.as_view(), name='api_data_export'),

    re_path('^admin/', admin.site.urls),
    path('accounts/', include('django.contrib.auth.urls')),
    #Check https://docs.djangoproject.com/en/2.2/topics/auth/default/#module-django.contrib.auth.views for all views added by the auth system
    # login, logout, password_change, password_reset
    #path('admin/login/', auth_views.LoginView.as_view(), name='login'),
    #path('accounts/logout/', auth_views.LogoutView.as_view(), name='logout'),
    path('accounts/profile/', PersonDetailView.as_view(), name='user_home'),
    ]
# TODO: We might want enable + django.conf.urls.static.static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT) for debug sessions?
