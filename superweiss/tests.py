# This file is part of Superweiss.
#
# Superweiss is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Superweiss is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Superweiss.  If not, see <http://www.gnu.org/licenses/>.
from datetime import datetime
from django.contrib.auth.models import User
from django.test import TestCase


from superweiss.models import Term, Chair, Person, Examination,\
    Supervision, Backup

class SupervisionNeededTestCase(TestCase):
    def setUp(self):
        user = User.objects.create_user('test', 'test@local.com', 'testpasswd')
        Examination.objects.create(subject="BWL", lecturer=user,
            starttime=datetime.strptime('2019-01-01 09:00+0000','%Y-%m-%d %H:%M%z'),
            endtime =datetime.strptime('2019-01-01 10:30+0000','%Y-%m-%d %H:%M%z'),
            num_supervisors=2)


    def test_lecture_characteristics(self):
        """Test simple Examination characteristics and helpers"""
        bwl = Examination.objects.get(subject="BWL")
        self.assertEqual(bwl.duration, 90)
        # No assigned supervisions yet...
        self.assertEqual(bwl.supervision_set.count(), 0)
        self.assertEqual(bwl.signup_needed(), True)
        self.assertEqual(bwl.backups_needed(), True)
